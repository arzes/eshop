package com.zx.eshop.eshop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/eshop/")
public class TestController {


    @RequestMapping(value = "index", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String index() throws Exception {
        return "welcome eshop";
    }
}
